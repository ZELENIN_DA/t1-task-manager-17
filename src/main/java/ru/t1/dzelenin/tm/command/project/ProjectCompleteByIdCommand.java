package ru.t1.dzelenin.tm.command.project;

import ru.t1.dzelenin.tm.enumerated.Status;
import ru.t1.dzelenin.tm.util.TerminalUtil;

public final class ProjectCompleteByIdCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[COMPLETE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        getProjectService().changeProjectStatusId(id, Status.COMPLETED);
    }

    @Override
    public String getName() {
        return "project-change-status-by-id";
    }

    @Override
    public String getDescription() {
        return "Complete project by id.";
    }

    @Override
    public String getArgument() {
        return null;
    }

}
