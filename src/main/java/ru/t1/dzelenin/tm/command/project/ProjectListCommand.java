package ru.t1.dzelenin.tm.command.project;

import ru.t1.dzelenin.tm.enumerated.Sort;
import ru.t1.dzelenin.tm.model.Project;
import ru.t1.dzelenin.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectListCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(Sort.values()));
        final String sortType = TerminalUtil.nextLine();
        final Sort sort = Sort.toSort(sortType);
        final List<Project> projects = getProjectService().findAll(sort);
        final StringBuilder stringBuilder = new StringBuilder();
        int index = 1;
        for (final Project project : projects) {
            if (project == null) continue;
            stringBuilder.append(index + ".");
            stringBuilder.append(project.getName() + " : ");
            stringBuilder.append(project.getDescription() + " : ");
            stringBuilder.append(project.getId());
            System.out.println(stringBuilder);
            index++;
            stringBuilder.setLength(0);
        }
    }

    @Override
    public String getName() {
        return "project-list";
    }

    @Override
    public String getDescription() {
        return "Show list projects.";
    }

    @Override
    public String getArgument() {
        return null;
    }

}
