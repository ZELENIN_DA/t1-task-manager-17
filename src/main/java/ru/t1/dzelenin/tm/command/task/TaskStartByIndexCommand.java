package ru.t1.dzelenin.tm.command.task;

import ru.t1.dzelenin.tm.enumerated.Status;
import ru.t1.dzelenin.tm.util.TerminalUtil;

public final class TaskStartByIndexCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[START TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        getTaskService().changeTaskStatusIndex(index, Status.IN_PROGRESS);
    }

    @Override
    public String getName() {
        return "task-start-by-index";
    }

    @Override
    public String getDescription() {
        return "Start task by index.";
    }

    @Override
    public String getArgument() {
        return null;
    }

}
