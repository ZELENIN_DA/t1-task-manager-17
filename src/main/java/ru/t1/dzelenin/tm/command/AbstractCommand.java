package ru.t1.dzelenin.tm.command;

import ru.t1.dzelenin.tm.api.model.ICommand;
import ru.t1.dzelenin.tm.api.service.IServiceLocator;

public abstract class AbstractCommand implements ICommand {

    protected IServiceLocator serviceLocator;

    public abstract void execute();

    public abstract String getName();

    public abstract String getDescription();

    public abstract String getArgument();

    public IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    public void setServiceLocator(IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public String toString() {
        final String name = getName();
        final String description = getDescription();
        final String argument = getArgument();
        String result = "";
        if (name != null && !name.isEmpty()) result += name + " : ";
        if (description != null && !description.isEmpty()) result += description + " : ";
        if (argument != null && !argument.isEmpty()) result += argument + " : ";
        return result;
    }

}
