package ru.t1.dzelenin.tm.api.model;

import ru.t1.dzelenin.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status created);

}
